Pod::Spec.new do |s|
  s.name         = 'MediaLibraryKit'
  s.version      = '0.0.1'
  s.summary      = 'Media Library Kit for iOS'
  s.homepage     = 'https://github.com/pdherbemont/MediaLibraryKit'
  s.author       = { "Pierre d'Herbemont" => "pdherbemont@free.fr" }
  s.license      = { type: 'MIT' }
  s.platform = :ios, "6.0"
  s.source = {
    :git => 'git://git.videolan.org/MediaLibraryKit.git',
    :commit => 'eb2587e0c2220ce6766762e4599f3ddbfe86996b'
  }
  s.header_mappings_dir = './'
  s.requires_arc = false
  s.prefix_header_contents = ''
  s.public_header_files = "Headers/Public/*.h"
  s.private_header_files = "Headers/Internal/*.h"
  s.source_files = '{Sources,Headers}/**/*.{h,m}'
  s.prefix_header_file = 'MobileMediaLibraryKit_Prefix.pch'
  s.library = 'xml2'
  s.xcconfig = { 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2' }
  s.dependency   'TouchXML'
  s.dependency   'MobileVLCKit'
end
